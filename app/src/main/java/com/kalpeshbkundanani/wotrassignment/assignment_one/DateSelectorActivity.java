package com.kalpeshbkundanani.wotrassignment.assignment_one;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.kalpeshbkundanani.wotrassignment.R;
import com.kalpeshbkundanani.wotrassignment.adapters.DateSelectorAdapter;
import com.kalpeshbkundanani.wotrassignment.utils.CalenderUtils;

import java.util.Calendar;
import java.util.List;

public class DateSelectorActivity extends AppCompatActivity {

    private DateSelectorViewModel dateSelectorViewModel;
    private RecyclerView monthsRecyclerView;
    private DateSelectorAdapter monthsListAdapter;
    private RecyclerView datesRecyclerView;
    private DateSelectorAdapter datesAdapter;
    private RecyclerView yearsRecyclerView;
    private DateSelectorAdapter yearsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_selector);

        initViewModel();
        initViews();
    }

    private void initViewModel() {
        final DateSelectorViewModel.Factory viewModelFactory
                = new DateSelectorViewModel.Factory();

        final ViewModelProvider viewModelProvider
                = new ViewModelProvider(this, viewModelFactory);

        dateSelectorViewModel = viewModelProvider.get(DateSelectorViewModel.class);
    }

    private void initViews() {
        initActionBar();
        initMonthsSelectorView();
        initDatesChangeObserver();
        initYearsSelectorView();
        initDaysSelectionSeekBar();
        initNextButton();
    }

    /**
     * Will initialize date selection.
     */
    private void initDatesChangeObserver() {
        dateSelectorViewModel.datesList.observe(this, this::updateDates);
    }

    /**
     * Will change the list of dates according to month selection.
     *
     * @param dates: days of months.
     */
    private void updateDates(List<Integer> dates) {
        if (datesAdapter == null) {
            initDatesSelectorView(dates);
        } else {
            datesAdapter.updateDataSet(dates);
        }
    }

    private void initMonthsSelectorView() {
        // Layout to inflate title and month list.
        final View monthsLayout = findViewById(R.id.month_date_time_layout);

        // Title text view.
        final TextView monthTitle = monthsLayout.findViewById(R.id.title_text_view);
        monthTitle.setText(getString(R.string.title_month));

        // Initializing months recycler view.
        monthsRecyclerView = monthsLayout.findViewById(R.id.date_time_options_recycler_view);
        // Initializing months recycler view adapter.
        final Drawable selectedDrawable = ContextCompat.getDrawable(this, R.drawable.selected_capsule);
        final Drawable unSelectedDrawable = ContextCompat.getDrawable(this, R.drawable.un_selected_capsule);
        monthsListAdapter = new DateSelectorAdapter(dateSelectorViewModel.monthsList, selectedDrawable, unSelectedDrawable, CalenderUtils::toMonthString);

        // setting listener that can update view model on item click.
        monthsListAdapter.setOnSelectionChangeListener(dateSelectorViewModel::setMonth);

        // setting observer to observe month change.
        dateSelectorViewModel.monthSelection.observe(this, integer -> monthsListAdapter.setSelection(monthsRecyclerView, integer));

        // used for inflating recycler view horizontally and scrolling to selected date.
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        monthsRecyclerView.setLayoutManager(mLayoutManager);
        monthsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // assigning initialized adapter to recycler view.
        monthsRecyclerView.setAdapter(monthsListAdapter);
    }

    private void initDatesSelectorView(List<Integer> dates) {
        // Layout to inflate title and date list.
        final View datesLayout = findViewById(R.id.date_date_time_layout);

        // Title text view.
        final TextView dateTitle = datesLayout.findViewById(R.id.title_text_view);
        dateTitle.setText(getString(R.string.title_date));

        // Initializing dates recycler view.
        datesRecyclerView = datesLayout.findViewById(R.id.date_time_options_recycler_view);
        // Initializing dates recycler view adapter.
        final Drawable selectedDrawable = ContextCompat.getDrawable(this, R.drawable.selected_circle);
        final Drawable unSelectedDrawable = ContextCompat.getDrawable(this, R.drawable.un_selected_circle);
        datesAdapter = new DateSelectorAdapter(dates, selectedDrawable, unSelectedDrawable);

        // setting listener that can update view model on item click.
        datesAdapter.setOnSelectionChangeListener(dateSelectorViewModel::setDate);

        // setting observer to observe date change.
        dateSelectorViewModel.dateSelection.observe(this, integer -> datesAdapter.setSelection(datesRecyclerView, integer));

        // used for inflating recycler view horizontally and scrolling to selected date.
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        datesRecyclerView.setLayoutManager(mLayoutManager);
        datesRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // assigning initialized adapter to recycler view.
        datesRecyclerView.setAdapter(datesAdapter);
    }

    private void initYearsSelectorView() {
        // Layout to inflate title and year list.
        final View yearsLayout = findViewById(R.id.year_date_time_layout);

        // Title text view.
        final TextView yearTitle = yearsLayout.findViewById(R.id.title_text_view);
        yearTitle.setText(getString(R.string.title_year));

        // Initializing years recycler view.
        yearsRecyclerView = yearsLayout.findViewById(R.id.date_time_options_recycler_view);
        // Initializing years recycler view adapter.
        final Drawable selectedDrawable = ContextCompat.getDrawable(this, R.drawable.selected_capsule);
        final Drawable unSelectedDrawable = ContextCompat.getDrawable(this, R.drawable.un_selected_capsule);
        yearsAdapter = new DateSelectorAdapter(dateSelectorViewModel.yearsList, selectedDrawable, unSelectedDrawable);

        // setting listener that can update view model on item click.
        yearsAdapter.setOnSelectionChangeListener(dateSelectorViewModel::setYear);

        // setting observer to observe year change.
        dateSelectorViewModel.yearSelection.observe(this, integer -> yearsAdapter.setSelection(yearsRecyclerView, integer));

        // used for inflating recycler view horizontally and scrolling to selected date.
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        yearsRecyclerView.setLayoutManager(mLayoutManager);
        yearsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // assigning initialized adapter to recycler view.
        yearsRecyclerView.setAdapter(yearsAdapter);
    }

    private void initDaysSelectionObserver() {
        final TextView numberOfDaysSelectedView = findViewById(R.id.number_of_days_selected_text_view);
        dateSelectorViewModel.numberOfDaysSelected.observe(this, daysValue -> {
            String text = daysValue + " days";
            numberOfDaysSelectedView.setText(text);
        });
    }

    /**
     * Used for sowing days selection.
     */
    private void initDaysSelectionSeekBar() {
        final SeekBar daysSelection = findViewById(R.id.day_selector_seek_bar);
        daysSelection.setOnSeekBarChangeListener(onDaysSelectionChangeListener);
        initDaysSelectionObserver();
    }

    /**
     * Will change the text that shows the days selected for sowing.
     */
    private final SeekBar.OnSeekBarChangeListener onDaysSelectionChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            dateSelectorViewModel.updateDaysSelection(progress);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    private void initActionBar() {
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setLogo(R.drawable.ic_account_circle);
        }
    }

    private void initNextButton() {
        final FloatingActionButton fab = findViewById(R.id.next_page_floating_action_button);
        fab.setOnClickListener(this::onNextButtonClicked);
    }

    private void onNextButtonClicked(View view) {
        final Calendar calendar = dateSelectorViewModel.getSelectedDate();
        int days = dateSelectorViewModel.getNumberOfDays();
        final Intent intent = new Intent(this, NextPageActivity.class);
        intent.putExtra(IntentKeys.SELECTED_DATE_MILLS, calendar.getTimeInMillis());
        intent.putExtra(IntentKeys.SELECTED_SOWING_DAYS, days);
        startActivity(intent);
    }
}
