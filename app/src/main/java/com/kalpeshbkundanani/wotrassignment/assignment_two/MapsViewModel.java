package com.kalpeshbkundanani.wotrassignment.assignment_two;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Stack;

/**
 * This class is used to hold data for MapsActivity.
 * Change in data is notified to observers.
 *
 * @author : Kalpesh Kundanani.
 */
class MapsViewModel extends ViewModel {

    // Used to keep track of user's current or user's selection location.
    final MutableLiveData<LatLng> userLocationLatLngLiveData = new MutableLiveData<>();
    // Used to notify views that all markers are removed from the map.
    final MutableLiveData<Boolean> stackEmptyLiveData = new MutableLiveData<>();
    // Used to notify view that user can now move to next page.
    final MutableLiveData<Boolean> showNextPageButton = new MutableLiveData<>();
    // Used to draw PolyLine or PolyGon on map according to number of elements.
    final MutableLiveData<ArrayList<LatLng>> latLngListLiveData = new MutableLiveData<>();
    // Used to undo the addition of marker on map.
    private final Stack<LatLng> userSelectionOfArea = new Stack<>();

    public MapsViewModel() {
        // initially there are no markers.
        stackEmptyLiveData.postValue(true);

        // initially user should not be given
        // an option to go to next page.
        showNextPageButton.postValue(false);
    }

    /**
     * Will record the user's selected location on the map.
     *
     * @param latLng: location where marker is added on map.
     */
    void postNewLatLng(LatLng latLng) {
        // used to save user's selected location.
        userLocationLatLngLiveData.postValue(latLng);
    }

    /**
     * Will record the user's selected location on the map.
     *
     * @param location: location where marker is added on map.
     */
    void postNewLocation(Location location) {
        // used to save user's selected location.
        final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        postNewLatLng(latLng);
    }

    /**
     * Will record the last added marker on the map.
     * also it will notify views that a new marker is
     * added to the view.
     *
     * @param latLng: location where marker is added on map.
     */
    void addSelection(LatLng latLng) {
        // save marker in stack, which can be un-done using undo button.
        userSelectionOfArea.push(latLng);

        // notifies views about stack's size.
        updateStackStatus();
    }

    private void updateStackStatus() {
        boolean isLatLngStackEmpty = userSelectionOfArea.empty();
        if (!isLatLngStackEmpty) {
            // if there are 3 or more markers polygon can be created.
            // if polygon can be created then users can go to next
            // page to find it's area.
            int size = userSelectionOfArea.size();
            boolean shouldShowNextButton = size >= 3;
            showNextPageButton.postValue(shouldShowNextButton);
        } else {
            // if there are no markers then user can't go to next page.
            showNextPageButton.postValue(false);
        }
        // used to draw polygon or polyline depending on number of elements.
        latLngListLiveData.postValue(new ArrayList<>(userSelectionOfArea));

        // notifies views that there are no markers.
        // will turn off undo option.
        stackEmptyLiveData.postValue(isLatLngStackEmpty);
    }

    /**
     * @return : JSON of LatLng data selected by user on map.
     */
    String getLatLngJSON() {
        // take previously posted value from live data.
        final ArrayList<LatLng> latLngs = latLngListLiveData.getValue();
        // JSONArray of JSONObjects where JSONObjects are LatLngs.
        final JSONArray latLngJsonArray = new JSONArray();
        // if the data was previously posted to the list.
        if (latLngs != null) {
            // iterate over all selected latLngs.
            for (final LatLng latLng : latLngs) {
                try {
                    // Serialize LatLng to JSON.
                    final JSONObject latLngJson = new JSONObject();
                    latLngJson.put("Lat", latLng.latitude);
                    latLngJson.put("Lng", latLng.longitude);
                    latLngJsonArray.put(latLngJson);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return latLngJsonArray.toString();
    }

    /**
     * Will remove the last added marker from the map.
     */
    void undoAddSelection() {
        // removing last added marker from the stack.
        boolean isLatLngStackEmpty = userSelectionOfArea.empty();
        if (!isLatLngStackEmpty) {
            // removing last entered element of the stack.
            userSelectionOfArea.pop();

            // posting stack status.
            updateStackStatus();
        }
    }

    /**
     * @return : instance of the latLng last added in the list.
     */
    LatLng lastLatLng() {
        // returning values of top element in the stack.
        return userSelectionOfArea.peek();
    }

    /**
     * Removes all markers from the stack.
     */
    void clearStack() {
        // removing all elements from the stack.
        userSelectionOfArea.clear();

        // notifying all the observer's of stack that it is cleared.
        updateStackStatus();
    }

    /**
     * Used to create ViewModel Instance.
     */
    static class Factory implements ViewModelProvider.Factory {
        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            try {
                return modelClass.newInstance();
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Cannot create instance of " + modelClass, e);
            } catch (InstantiationException e) {
                throw new RuntimeException("Cannot create instance of " + modelClass, e);
            }
        }
    }
}
