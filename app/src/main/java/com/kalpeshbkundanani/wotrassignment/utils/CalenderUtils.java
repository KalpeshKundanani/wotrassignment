package com.kalpeshbkundanani.wotrassignment.utils;

import java.text.DateFormatSymbols;

public class CalenderUtils {

    public static String toMonthString(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }
}
