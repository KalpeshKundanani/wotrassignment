package com.kalpeshbkundanani.wotrassignment.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides utils required for List operations.
 */
public class ListUtils {
    /**
     * @param from: number at first index of the list.
     * @param upTo: number at last index of the list.
     * @return List of size upTo - from.
     */
    public static List<Integer> generateIntList(int from, int upTo) {
        final List<Integer> intList = new ArrayList<>();
        for (int i = from; i <= upTo; i++) {
            intList.add(i);
        }
        return intList;
    }
}
