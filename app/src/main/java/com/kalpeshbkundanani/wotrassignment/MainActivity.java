package com.kalpeshbkundanani.wotrassignment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.kalpeshbkundanani.wotrassignment.assignment_one.DateSelectorActivity;
import com.kalpeshbkundanani.wotrassignment.assignment_two.MapsActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onAssignmentOneClicked(View view) {
        final Intent intent = new Intent(this, DateSelectorActivity.class);
        startActivity(intent);
    }

    public void onAssignmentTwoClicked(View view) {
        final Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }
}
