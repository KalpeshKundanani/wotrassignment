package com.kalpeshbkundanani.wotrassignment.adapters;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kalpeshbkundanani.wotrassignment.R;
import com.kalpeshbkundanani.wotrassignment.functional_interfaces.Consumer;
import com.kalpeshbkundanani.wotrassignment.functional_interfaces.Function;

import java.util.List;

public class DateSelectorAdapter extends RecyclerView.Adapter<DateSelectorAdapter.ViewHolder> {

    private final Function<Integer, String> integerStringFunction;
    private final Drawable selectedBackground;
    private final Drawable unSelectedBackground;
    private List<Integer> dataSet;
    private Integer selectedItem;
    private Consumer<Integer> onSelectionChangeListener;

    public DateSelectorAdapter(List<Integer> dataSet, Drawable selectedBackground, Drawable unSelectedBackground) {
        this(dataSet, selectedBackground, unSelectedBackground, null);
    }

    public DateSelectorAdapter(List<Integer> dataSet, Drawable selectedBackground, Drawable unSelectedBackground, Function<Integer, String> integerStringFunction) {
        this.dataSet = dataSet;
        this.integerStringFunction = integerStringFunction;
        this.selectedBackground = selectedBackground;
        this.unSelectedBackground = unSelectedBackground;
        selectedItem = 0;
    }

    public void setOnSelectionChangeListener(Consumer<Integer> onSelectionChangeListener) {
        this.onSelectionChangeListener = onSelectionChangeListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.date_list_item_layout, parent, false);
        return new DateSelectorAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Integer currentItemData = dataSet.get(position);
        String text;

        if (integerStringFunction == null) text = String.valueOf(currentItemData);
        else text = integerStringFunction.apply(currentItemData);

        final TextView textView = holder.textView;
        textView.setText(text);
        final Drawable textDrawable = isSelected(position) ? selectedBackground : unSelectedBackground;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            textView.setBackground(textDrawable);
        } else {
            textView.setBackgroundDrawable(textDrawable);
        }
        textView.setOnClickListener(v -> setSelection(position));
    }

    private void setSelection(int position) {
        if (selectedItem == position) return;
        selectedItem = position;
        if (onSelectionChangeListener != null) {
            onSelectionChangeListener.accept(dataSet.get(position));
        }
        notifyDataSetChanged();
    }

    public void setSelection(RecyclerView recyclerView, int value) {
        int position = dataSet.indexOf(value);
        if (selectedItem == position) return;
        if (position >= 0) {
            selectedItem = position;
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            if (layoutManager != null) {
                layoutManager.scrollToPosition(position);
            }
            notifyDataSetChanged();
        }
    }

    private boolean isSelected(int position) {
        return position == selectedItem;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void updateDataSet(List<Integer> integers) {
        this.dataSet = integers;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.date_text_view);
        }
    }
}
