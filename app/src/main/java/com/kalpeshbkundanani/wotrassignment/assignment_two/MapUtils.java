package com.kalpeshbkundanani.wotrassignment.assignment_two;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import com.kalpeshbkundanani.wotrassignment.R;

import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.LOCATION_SERVICE;

/**
 * This class is used to provide drawing utility to the MapsActivity.java
 *
 * @author : Kalpesh Kundanani.
 */
class MapUtils {

    private static Marker userLocationMarker;
    private static MarkerOptions userLocationMarkerOption;
    private static Circle mapCircle;
    private static final HashMap<LatLng, Marker> markerRecordMap = new HashMap<>();
    private static Polyline polyline;
    private static Polygon polygon;
    private static CircleOptions mapCircleOptions;
    private static PolygonOptions polygonOptions;
    private static PolylineOptions polylineOptions;
    private static double computedArea;

    /**
     * @return : last calculated area by PolyGon.
     */
    static double getComputedArea() {
        return computedArea;
    }

    @SuppressLint("MissingPermission")
    static Location getLocation(Context mContext) {
        try {
            final LocationManager locationManager =
                    (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
            if (locationManager != null) {
                // get GPS status
                boolean isGPSAvailable = locationManager
                        .isProviderEnabled(LocationManager.GPS_PROVIDER);
                // get network provider status
                boolean isNetworkAvailable = locationManager
                        .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                if (!isGPSAvailable && !isNetworkAvailable) {
                    Toast.makeText(mContext, "No Service Provider is available", Toast.LENGTH_SHORT).show();
                } else {
                    // if GPS Enabled get lat/long using GPS Services
                    if (isGPSAvailable) {
                        return locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Moves focus and marker to the provided LatLng
     */
    static void locateTo(Context context, GoogleMap mMap, LatLng latLng) {
        if (latLng == null) return;
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18.5f));
        drawStroke(context, mMap, latLng);
        drawUserLocationMarker(context, mMap, latLng);
    }

    /**
     * Draws marker to indicate user's position on map.
     */
    private static void drawUserLocationMarker(Context context, GoogleMap mMap, LatLng latLng) {
        final Drawable circleDrawable = context.getResources().getDrawable(R.drawable.user_location_marker);
        final BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);

        userLocationMarkerOption = new MarkerOptions()
                .position(latLng)
                .icon(markerIcon);
        if (userLocationMarker != null) {
            userLocationMarker.remove();
        }
        userLocationMarker = mMap.addMarker(userLocationMarkerOption);
    }

    /**
     * Returns custom marker bitmap.
     */
    private static BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    /**
     * Draws marker on the map.
     */
    static void addMarker(GoogleMap mMap, LatLng latLng) {
        if (!markerRecordMap.containsKey(latLng)) {
            final MarkerOptions icon = new MarkerOptions()
                    .position(latLng);
            Marker marker = mMap.addMarker(icon);
            markerRecordMap.put(latLng, marker);
        }
    }

    /**
     * Removes marker from the map.
     */
    static void removeMarker(LatLng latLng) {
        if (markerRecordMap.containsKey(latLng)) {
            final Marker marker = markerRecordMap.get(latLng);
            if (marker != null) {
                marker.remove();
            }
            markerRecordMap.remove(latLng);
        }
    }

    private static void drawStroke(Context context, GoogleMap mMap, LatLng latLng) {
        // Instantiating CircleOptions to draw a circle around the userLocationMarker
        mapCircleOptions = new CircleOptions();

        // Specifying the center of the circle
        mapCircleOptions.center(latLng);

        // Radius of the circle
        mapCircleOptions.radius(50);

        // Border color of the circle
        int color = ContextCompat.getColor(context, R.color.map_located_area_color);
        mapCircleOptions.strokeColor(color);

        // Border width of the circle
        mapCircleOptions.strokeWidth(8);

        if (mapCircle != null) {
            mapCircle.remove();
        }

        // Adding the circle to the GoogleMap
        mapCircle = mMap.addCircle(mapCircleOptions);
    }

    static void removePolyLine(GoogleMap mMap) {
        if (polyline != null) {
            polyline.remove();
            polyline = null;
            redrawMap(mMap);
        }
    }

    static void removePolyGon(GoogleMap mMap) {
        if (polygon != null) {
            polygon.remove();
            polygon = null;
            computedArea = -1;
            redrawMap(mMap);
        }
    }

    static void addPolyLine(GoogleMap mMap, ArrayList<LatLng> latLngs) {
        // Add a polyline to the map.
        polylineOptions = (new PolylineOptions())
                .clickable(true)
                .addAll(latLngs);
        polyline = mMap.addPolyline(polylineOptions);
        redrawMap(mMap);
    }

    static void addPolyGon(GoogleMap mMap, ArrayList<LatLng> latLngs) {
        computedArea = SphericalUtil.computeArea(latLngs);
        polygonOptions = new PolygonOptions()
                .clickable(true)
                .addAll(latLngs);
        polygon = mMap.addPolygon(polygonOptions);
        // Store a data object with the polygon, used here to indicate an arbitrary type.
        polygon.setTag("alpha");
        redrawMap(mMap);
    }

    private static void redrawMap(GoogleMap mMap) {
        mMap.clear();
        if (userLocationMarkerOption != null) {
            userLocationMarker =
                    mMap.addMarker(userLocationMarkerOption);
        }

        if (mapCircleOptions != null) {
            mapCircle = mMap.addCircle(mapCircleOptions);
        }

        for (LatLng latLng : markerRecordMap.keySet()) {
            final MarkerOptions icon = new MarkerOptions()
                    .position(latLng);
            Marker marker = mMap.addMarker(icon);
            markerRecordMap.put(latLng, marker);
        }
        if (polygon != null) {
            polygon = mMap.addPolygon(polygonOptions);
            // Store a data object with the polygon, used here to indicate an arbitrary type.
            polygon.setTag("alpha");
        }

        if (polyline != null) {
            polyline = mMap.addPolyline(polylineOptions);
        }
    }

    static void clear(GoogleMap mMap) {
        mMap.clear();
        if (userLocationMarkerOption != null) {
            userLocationMarker =
                    mMap.addMarker(userLocationMarkerOption);
        }
        if (mapCircleOptions != null) {
            mapCircle = mMap.addCircle(mapCircleOptions);
        }
        markerRecordMap.clear();
    }
}
