package com.kalpeshbkundanani.wotrassignment.assignment_one;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.kalpeshbkundanani.wotrassignment.utils.ListUtils;

import java.util.Calendar;
import java.util.List;

class DateSelectorViewModel extends ViewModel {

    private final static int DAYS_INTERVAL = 20;
    private final static int MIN_DAYS = 60;
    private final static int FIRST_DAYS_SELECTION = MIN_DAYS - DAYS_INTERVAL;

    final MutableLiveData<Integer> monthSelection = new MutableLiveData<>();
    final MutableLiveData<Integer> dateSelection = new MutableLiveData<>();
    final MutableLiveData<Integer> yearSelection = new MutableLiveData<>();

    final List<Integer> monthsList = ListUtils.generateIntList(1, 12);
    final List<Integer> yearsList = ListUtils.generateIntList(2019, 2020);
    final MutableLiveData<Integer> numberOfDaysSelected = new MutableLiveData<>();

    final MutableLiveData<List<Integer>> datesList = new MutableLiveData<>();

    public DateSelectorViewModel() {
        final Calendar calendar = Calendar.getInstance();

        final int thisYear = calendar.get(Calendar.YEAR);
        setYear(thisYear);

        final int thisMonth = calendar.get(Calendar.MONTH) + 1;
        setMonth(thisMonth);

        updateDaysSelection(0);
    }

    void updateDaysSelection(int progress) {
        progress++;
        numberOfDaysSelected.postValue(FIRST_DAYS_SELECTION + (progress * DAYS_INTERVAL));
    }

    int getNumberOfDays() {
        final Integer value = numberOfDaysSelected.getValue();
        if (value == null) {
            return MIN_DAYS;
        } else {
            return value;
        }
    }

    void setMonth(Integer newMonth) {
        monthSelection.postValue(newMonth);
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, newMonth - 1);
        int monthMaxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        final List<Integer> intList = ListUtils.generateIntList(1, monthMaxDays);
        datesList.postValue(intList);

        final Integer value = dateSelection.getValue();
        if (value == null) {
            int thisDate = calendar.get(Calendar.DATE);
            setDate(thisDate);
        } else {
            if (intList.contains(value)) {
                setDate(value);
            } else {
                setDate(intList.get(0));
            }
        }
    }

    void setDate(Integer newDate) {
        dateSelection.postValue(newDate);
    }

    void setYear(Integer newYear) {
        yearSelection.postValue(newYear);
    }

    Calendar getSelectedDate() {
        final Calendar calendar = Calendar.getInstance();
        final Integer month = monthSelection.getValue();
        final Integer date = dateSelection.getValue();
        final Integer year = yearSelection.getValue();
        if (month != null) {
            calendar.set(Calendar.MONTH, month - 1);
        }
        if (date != null) {
            calendar.set(Calendar.DATE, date);
        }
        if (year != null) {
            calendar.set(Calendar.YEAR, year);
        }
        return calendar;
    }

    static class Factory implements ViewModelProvider.Factory {
        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            try {
                return modelClass.newInstance();
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Cannot create instance of " + modelClass, e);
            } catch (InstantiationException e) {
                throw new RuntimeException("Cannot create instance of " + modelClass, e);
            }
        }
    }
}
