package com.kalpeshbkundanani.wotrassignment.assignment_two;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.kalpeshbkundanani.wotrassignment.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

/**
 * This activity is used to display selected Area and
 * LatLngs from the MapsActivity
 *
 * @author : Kalpesh Kundanani.
 */
public class NextPageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_next_page);
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initActionBar();
        initResultTextView();
    }

    private void initActionBar() {
        final ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initResultTextView() {
        final Intent intent = getIntent();
        final Bundle extras = intent.getExtras();
        if (extras != null) {
            final TextView textView = findViewById(R.id.result_text_view);
            String result = getResultString(extras);
            textView.setText(result);
        }
    }

    /**
     * Parses Intent and returns displayable string.
     * @param extras: data from intent.
     * @return : String that is displayable to user.
     */
    private String getResultString(Bundle extras) {
        final double computedArea = extras.getDouble("computed_area");
        final String latLngs = getLatLngs(extras);

        String result = String.format(Locale.getDefault(), "Area: %.2f Square Meters", computedArea);
        result += latLngs;
        return result;
    }

    /**
     * Parses JSON and returns displayable string.
     * @param extras: data from intent.
     * @return : String that is displayable to user.
     */
    private String getLatLngs(Bundle extras) {
        final String latLngsJSON = extras.getString("lat_lngs");
        final StringBuilder result = new StringBuilder("\n\n");
        try {
            final JSONArray jsonArray = new JSONArray(latLngsJSON);
            for (int i = 0; i < jsonArray.length(); i++) {
                final JSONObject jsonObject = jsonArray.getJSONObject(i);
                final double lat = jsonObject.getDouble("Lat");
                final double lng = jsonObject.getDouble("Lng");

                result.append(i + 1).append(". \nLat: ")
                        .append(lat).append("\nLng: ")
                        .append(lng).append("\n\n");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

}
