package com.kalpeshbkundanani.wotrassignment.assignment_one;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.kalpeshbkundanani.wotrassignment.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Screen that displays the selected date and sowing
 * days selected by user from DateSelectorActivity
 *
 * @author Kalpesh Kundanani.
 */
public class NextPageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_page);

        initActionBar();
        showResult();
    }

    private void initActionBar() {
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // will show the back button in action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void showResult() {
        final Bundle extras = getIntent().getExtras();
        if (extras != null) {
            // get data from intent.
            final long selectedDateInMills = extras.getLong(IntentKeys.SELECTED_DATE_MILLS);
            final int selectedSowingDays = extras.getInt(IntentKeys.SELECTED_SOWING_DAYS);

            // create result text from the data received.
            String resultText = getDateString(selectedDateInMills);
            resultText += "\n" + selectedSowingDays + " days";

            // display data.
            final TextView textView = findViewById(R.id.result_text_view);
            textView.setText(resultText);
        }
    }

    /**
     * Converts time in mills to a formatted string.
     * Date format: dd-MMMM-yyyy
     *
     * @param selectedDateInMills: time in milli seconds since epoch.
     * @return formatted date.
     */
    private String getDateString(long selectedDateInMills) {
        // For formatting date.
        final SimpleDateFormat formatter = new SimpleDateFormat("dd-MMMM-yyyy", Locale.getDefault());

        // Calendar object to convert the date value in milliseconds to date.
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(selectedDateInMills);
        return formatter.format(calendar.getTime());
    }

}
