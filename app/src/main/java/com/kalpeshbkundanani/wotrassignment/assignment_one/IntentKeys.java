package com.kalpeshbkundanani.wotrassignment.assignment_one;

class IntentKeys {
    public final static String SELECTED_DATE_MILLS = "selected_date_mills";
    public final static String SELECTED_SOWING_DAYS = "selected_sowing_days";
}
