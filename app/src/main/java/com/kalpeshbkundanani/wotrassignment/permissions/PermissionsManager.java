package com.kalpeshbkundanani.wotrassignment.permissions;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.kalpeshbkundanani.wotrassignment.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static android.text.TextUtils.isEmpty;

public final class PermissionsManager {

    private static Toast toast = null;

    private String reasonForAskingPermissions = "This app needs some permissions to function properly." +
            " Please continue and allow all permissions.";
    private AlertDialog.Builder alertDialogBuilder = null;

    private static final String PREF_KEY_PERMANENTLY_DENIED_DIALOGS = "permanently_denied_dialogs";

    private int permissionDialogCode;

    private String negativeButtonString;
    private NegativeButtonListener negativeButtonListener = null;

    interface NegativeButtonListener {

        void onNegativeButtonPressed();
    }
    public interface PermissionResponseListener {

        void onPermissionDenied(ArrayList<String> nonGrantedPermissions);

        void onPermissionGranted(ArrayList<String> grantedPermissions);

    }
    private PermissionsManager(Activity activity, int permissionDialogCode) {
        this.activity = activity;
        this.permissionDialogCode = permissionDialogCode;
    }

    private static final int PERMISSION_CALLBACK_CONSTANT = 100;

    private static final String PREF_KEY_PERMISSION_STATUS = "permission_status";
    private static final int REQUEST_PERMISSION_SETTING = 101;

    private final Activity activity;

    private PermissionResponseListener permissionResponseListener = null;
    private final ArrayList<String> requiredPermissions = new ArrayList<>();

    public static PermissionsManager forActivity(Activity activity) {
        return new PermissionsManager(activity, 0);
    }


    public PermissionsManager addReasonForPermissions(String message) {
        this.reasonForAskingPermissions = message;
        return this;
    }

    public PermissionsManager responseOnPostExecute(PermissionResponseListener permissionResponseListener) {
        this.permissionResponseListener = permissionResponseListener;
        return this;
    }

    public PermissionsManager askPermission(String requiredPermissions) {
        this.requiredPermissions.add(requiredPermissions);
        return this;
    }

    public PermissionsManager execute() {
        new Thread(() -> {
            try {
                final ArrayList<String> nonGrantedPermissions = getNonGrantedPermission();
                Log.d("newF", "execute: nonGrantedPermissions: " + nonGrantedPermissions);
                if (nonGrantedPermissions.size() > 0) {

                    if (!isPermissionAlreadyAsked()) {
                        //just request the permission
                        requestPermissionRuntime();
                    } else {
                        //Previously Permission Request was cancelled with 'Don't Ask Again',
                        // Redirect to Settings after showing Information about why you need the permission
                        showRequestPermissionDialog((dialog, which) -> requestPermissionSettings());
                    }

                    markPermissionAlreadyAsked();
                } else {
                    //You already have the permission, just go ahead.
                    onPermissionGranted(requiredPermissions);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        return this;
    }

    private boolean isPermissionAlreadyAsked() {
        return getPermissionPref().getBoolean(requiredPermissions.get(0), false);
    }

    private void markPermissionAlreadyAsked() {
        SharedPreferences.Editor editor = getPermissionPref().edit();
        editor.putBoolean(requiredPermissions.get(0), true);
        editor.apply();
    }

    private SharedPreferences getPermissionPref() {
        return activity.getSharedPreferences(PREF_KEY_PERMISSION_STATUS, MODE_PRIVATE);
    }

    private ArrayList<String> getNonGrantedPermission() {
        ArrayList<String> nonGrantedPermissions = new ArrayList<>();
        for (String permission : requiredPermissions) {
            if (isPermissionNotGranted(activity, permission)) {
                nonGrantedPermissions.add(permission);
            }
        }
        return nonGrantedPermissions;
    }

    private boolean shouldShowRequestPermissionRationale() {
        for (String permission : requiredPermissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                return true;
            }
        }
        return false;
    }

    private void showRequestPermissionDialog(DialogInterface.OnClickListener onPositiveButtonClickListener) {
        final boolean isNotDenied = isNotPermanentlyDeniedByUser();
        if (isNotDenied) {
            activity.runOnUiThread(getAlertDialog(onPositiveButtonClickListener)::show);
        }
    }

    private boolean isNotPermanentlyDeniedByUser() {
        SharedPreferences permissionPref = getPermissionPref();
        String permanentlyDeniedDialogs = permissionPref.getString(PREF_KEY_PERMANENTLY_DENIED_DIALOGS, "");

        return isEmpty(permanentlyDeniedDialogs) ||
                !permanentlyDeniedDialogs.contains("\"" + permissionDialogCode + "\"");
    }

    private void markDialogPermanentlyDenied() {
        final SharedPreferences permissionPref = getPermissionPref();
        final String permanentlyDeniedDialogs = permissionPref.getString(PREF_KEY_PERMANENTLY_DENIED_DIALOGS, "");

        try {
            final JSONArray jsonArray = (isEmpty(permanentlyDeniedDialogs)) ? new JSONArray() : new JSONArray(permanentlyDeniedDialogs);
            jsonArray.put(String.valueOf(permissionDialogCode));
            final SharedPreferences.Editor editor = permissionPref.edit();
            editor.putString(PREF_KEY_PERMANENTLY_DENIED_DIALOGS, jsonArray.toString())
                    .apply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private AlertDialog.Builder getAlertDialog(DialogInterface.OnClickListener onPositiveButtonClickListener) {
        final LayoutInflater layoutInflater = LayoutInflater.from(activity);
        final View neverShowView = layoutInflater.inflate(R.layout.dialog_checkbox_never_show, null);

        final CheckBox doNotShowAgainCheckbox = neverShowView.findViewById(R.id.never_show_checkbox);
        if (permissionDialogCode != 0) {
            doNotShowAgainCheckbox.setText(R.string.do_not_show_again);
        } else {
            doNotShowAgainCheckbox.setVisibility(View.GONE);
        }

        final TextView tvDescription = neverShowView.findViewById(R.id.tv_description);
        tvDescription.setText(Html.fromHtml(reasonForAskingPermissions));
        if (!shouldShowRequestPermissionRationale()){
            final String message = "<br/><br/>" +
                    activity.getString(R.string.go_to_permission_settings);
            tvDescription.append(Html.fromHtml(message));
        }

        if (alertDialogBuilder == null) {
            alertDialogBuilder = new AlertDialog.Builder(activity)
                    .setTitle("Required Permissions")
                    .setPositiveButton("Continue", onPositiveButtonClickListener)
                    .setOnDismissListener(dialog -> {
                        if (doNotShowAgainCheckbox.isChecked()) {
                            markDialogPermanentlyDenied();
                        }
                    })
                    .setCancelable(false);
            if (!isEmpty(negativeButtonString) && negativeButtonListener != null){
                alertDialogBuilder.setNegativeButton(negativeButtonString,
                        ((dialog, which) -> negativeButtonListener.onNegativeButtonPressed()));
            } else {
                alertDialogBuilder.setNegativeButton("Cancel", (dialog, which) -> respondForPermissionStatus());
            }

        }
        return alertDialogBuilder.setView(neverShowView);
    }

    private void respondForPermissionStatus() {
        final ArrayList<String> nonGrantedPermission = getNonGrantedPermission();
        onPermissionDenied(nonGrantedPermission);
        final ArrayList<String> grantedPermission = new ArrayList<>(requiredPermissions);
        grantedPermission.removeAll(nonGrantedPermission);
        onPermissionGranted(grantedPermission);
    }

    private void requestPermissionRuntime() {
        final ArrayList<String> nonGrantedPermission = getNonGrantedPermission();
        final String[] stringArray = nonGrantedPermission.toArray(new String[nonGrantedPermission.size()]);
        ActivityCompat.requestPermissions(activity, stringArray, PERMISSION_CALLBACK_CONSTANT);
    }

    private void requestPermissionSettings() {
        final Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        final Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        activity.startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
        toastIt(activity,activity.getString(R.string.go_to_permission_settings));
    }

    private void onPermissionGranted(ArrayList<String> grantedPermissions) {
        if (grantedPermissions.size() > 0) {
            activity.runOnUiThread(() -> permissionResponseListener.onPermissionGranted(grantedPermissions));
        }
    }

    private void onPermissionDenied(ArrayList<String> nonGrantedPermission) {
        if (nonGrantedPermission.size() > 0) {
            activity.runOnUiThread(() -> permissionResponseListener.onPermissionDenied(nonGrantedPermission));
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            ArrayList<String> nonGrantedPermissions = new ArrayList<>();
            //check if all permissions are granted
            for (int i = 0; i < grantResults.length; i++) {
                int grantResult = grantResults[i];
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    nonGrantedPermissions.add(requiredPermissions.get(i));
                }
            }

            final boolean allGranted = (nonGrantedPermissions.size() == 0);
            if (allGranted) {
                onPermissionGranted(requiredPermissions);
            } else if (shouldShowRequestPermissionRationale()) {
                showRequestPermissionDialog((dialog, which) -> requestPermissionRuntime());
            } else {
                toastIt(activity,"Unable to get Permission");
                permissionResponseListener.onPermissionDenied(nonGrantedPermissions);
            }
        }
    }

    public void onActivityResult(int requestCode) {
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            respondForPermissionStatus();
        }
    }

    private static boolean isPermissionNotGranted(Context context, String permission) {
        return ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED;
    }

    private static void toastIt(Context context, String message) {
        if (toast != null){
            toast.cancel();
            toast = null;
        }

        toast = Toast.makeText(context,message,Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public PermissionsManager setNegativeButton(String negativeButtonString, NegativeButtonListener negetiveButtonListener) {
        this.negativeButtonString = negativeButtonString;
        this.negativeButtonListener = negetiveButtonListener;
        return this;
    }
}
