package com.kalpeshbkundanani.wotrassignment.assignment_two;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.kalpeshbkundanani.wotrassignment.R;
import com.kalpeshbkundanani.wotrassignment.permissions.PermissionsManager;

import java.util.ArrayList;
import java.util.Arrays;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Screen where user can draw markers to select land area.
 * when user places more then three marker the area covered by
 * that marker can be calculated and will be shown to user
 * when they navigate to the next page.
 *
 * @author : Kalpesh Kundanani.
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    // Data provider for the views.
    private MapsViewModel mapsViewModel;

    // Instance of GoogleMap used to select land area.
    private GoogleMap mMap;

    // Used to get Location permission from the user.
    private PermissionsManager locationPermissionsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        initViewModel();
        initMapFragment();
    }

    /**
     * Initializes the GoogleMap.
     * when this is done, onMapReady callback is received.
     */
    private void initMapFragment() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        final SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        // used to search places by typing search query.
        initPlaces();
        // auto complete search fragment.
        initPlacesSearchView();

        askPermissionsToAccessLocation();

        // on change of user's selected location
        // camera and marker will move.
        initCurrentLocationLocator();

        // used to put marker's on screen.
        initOnMapClickListener();

        // used to remove last added marker from the screen.
        initUndoButton();

        // Used to remove all markers from the screen.
        initClearButton();

        // Will take users to the screen where
        // they can see the calculated area.
        initNextPageButton();

        // Used to draw/remove PolyLine or PolyGon on screen.
        initLatLngListObserver();
    }

    /**
     * Observe click events on map and draw markers there
     * records position of the marker so that it can
     * latter be used to calculate area of the land.
     */
    private void initOnMapClickListener() {
        // observe for click events on the map screen.
        mMap.setOnMapClickListener(latLng -> {
            // on click event add marker to that position.
            MapUtils.addMarker(mMap, latLng);

            // record the position so that it
            // can be used to calculate area.
            mapsViewModel.addSelection(latLng);
        });
    }

    /**
     * Shows button to the user, using which user can go to the
     * screen where they can see the calculated area and the LatLngs
     * where markers were added by the user.
     * <p>
     * This method toggles next button's visibility depending on
     * number of markers drawn on map.
     */
    private void initNextPageButton() {
        final Button nextPageButton = findViewById(R.id.next_page_button);

        // observing for the change in number of markers.
        mapsViewModel.showNextPageButton.observe(this, shouldShowNextButton -> {
            // will show button if marker >= 3 hide otherwise.
            int visibility = shouldShowNextButton ? View.VISIBLE : View.GONE;
            nextPageButton.setVisibility(visibility);
        });

        // on click of this button user will be asked for confirmation.
        nextPageButton.setOnClickListener(v -> createConfirmationDialog());
    }

    /**
     * This method will create dialog which will take confirmation
     * from user about the selection.
     * <p>
     * if they agree to continue they will be navigated to the result
     * string.
     * <p>
     * if they don't wish to continue they can clear the map and mark again.
     */
    private void createConfirmationDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Confirm Selection")
                .setMessage("Are you sure you want to select this area ?")
                .setCancelable(false)
                .setPositiveButton("Yes, sure.", (dialog, which) -> launchResultActivity())
                .setNeutralButton("Cancel", null)
                .setNegativeButton("No, want to draw again.", (dialog, which) -> clearMap())
                .show();
    }

    /**
     * removes all drawings from the map and add's user's location.
     */
    private void clearMap() {
        // remove recorded positions.
        mapsViewModel.clearStack();

        // remove drawings from the map.
        MapUtils.clear(mMap);
    }

    /**
     * Will navigate user to the result's activity from current screen.
     */
    private void launchResultActivity() {
        final Intent intent = new Intent(MapsActivity.this, NextPageActivity.class);
        double computedArea = MapUtils.getComputedArea();
        intent.putExtra("computed_area", computedArea);
        intent.putExtra("lat_lngs", mapsViewModel.getLatLngJSON());
        startActivity(intent);
    }

    /**
     * If number of markers drawn on map are:
     * 1. null: hide PolyLine and PolyGon.
     * 2. zero: hide PolyLine and PolyGon.
     * 2. one: hide PolyLine and PolyGon.
     * 3. one, two: draw PolyLine and hide PolyGon.
     * 3. three and above: draw PolyGon and hide PolyLine.
     */
    private void initLatLngListObserver() {
        mapsViewModel.latLngListLiveData.observe(this, latLngs -> {
            boolean noMarkersOnScreen = latLngs == null || latLngs.isEmpty();
            if (noMarkersOnScreen || latLngs.size() == 1) {
                MapUtils.removePolyLine(mMap);
            } else if (latLngs.size() < 3) { // one or two markers.
                MapUtils.removePolyGon(mMap);
                MapUtils.addPolyLine(mMap, latLngs);
            } else { // more than 2 markers.
                MapUtils.removePolyLine(mMap);
                MapUtils.addPolyGon(mMap, latLngs);
            }
        });
    }

    /**
     * Button used to remove all the markers from the screen.
     */
    private void initClearButton() {
        final Button clearButton = findViewById(R.id.clear_button);

        // observer if all markers are removed from the screen.
        mapsViewModel.stackEmptyLiveData.observe(this, isStackEmpty -> {
            // if all are removed then hide button.
            int undoButtonVisibility = isStackEmpty ? View.GONE : View.VISIBLE;
            clearButton.setVisibility(undoButtonVisibility);
        });

        // remove all data from map on click.
        clearButton.setOnClickListener(v -> {
            MapUtils.clear(mMap);
            mapsViewModel.clearStack();
        });
    }

    /**
     * This button is used to remove the last entered marker from the Map.
     */
    private void initUndoButton() {
        final Button undoButton = findViewById(R.id.undo_button);

        // visible till there is any element in the stack.
        mapsViewModel.stackEmptyLiveData.observe(this, isStackEmpty -> {
            int undoButtonVisibility = isStackEmpty ? View.GONE : View.VISIBLE;
            undoButton.setVisibility(undoButtonVisibility);
            if (isStackEmpty) {
                // clears the resources from the MapUtils.
                MapUtils.clear(mMap);
            }
        });
        // removes marker when clicked.
        undoButton.setOnClickListener(v -> {
            // remove from map.
            MapUtils.removeMarker(mapsViewModel.lastLatLng());
            // remove from ViewModel.
            mapsViewModel.undoAddSelection();
        });
    }

    /**
     * Observes current selected location by user and
     * moves camera and marker to new position when changed.
     */
    private void initCurrentLocationLocator() {
        mapsViewModel.userLocationLatLngLiveData.observe(this,
                latLng -> MapUtils.locateTo(this, mMap, latLng));
    }

    /**
     * RunTime permission model.
     * 1. Asks permissions if not already given.
     * 2. if already asked but denied then informs user about the reason and
     * redirects to settings page.
     * 3. when permission is granted, accesses user's location and
     * moves camera and marker to that position.
     */
    private void askPermissionsToAccessLocation() {
        locationPermissionsManager = PermissionsManager
                .forActivity(this)
                .askPermission(ACCESS_FINE_LOCATION)
                .addReasonForPermissions("To use maps we will need your location permission.")
                .responseOnPostExecute(new PermissionsManager.PermissionResponseListener() {
                    @Override
                    public void onPermissionDenied(ArrayList<String> nonGrantedPermissions) {
                    }

                    @Override
                    public void onPermissionGranted(ArrayList<String> grantedPermissions) {
                        final Location location = MapUtils.getLocation(MapsActivity.this);
                        if (location != null) {
                            mapsViewModel.postNewLocation(location);
                        }
                    }
                }).execute();
    }

    /**
     * Initializes fragment that is used to search places and provide suggestions
     * for places.
     */
    private void initPlacesSearchView() {
        final AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        if (autocompleteFragment != null) {
            // telling places sdk that we want ID, name and LatLng when a place is searched.
            autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));
            // call back fired from the fragment when places are searched.
            autocompleteFragment.setOnPlaceSelectedListener(placeSelectionListener);
        }
    }

    /**
     * Listener used to receive callback for AutocompleteSupportFragment.
     */
    private final PlaceSelectionListener placeSelectionListener = new PlaceSelectionListener() {
        @Override
        public void onPlaceSelected(@NonNull Place place) {
            final LatLng latLng = place.getLatLng();
            if (latLng != null) {
                mapsViewModel.postNewLatLng(latLng);
            } else {
                Toast.makeText(MapsActivity.this, "Not able to locate to " + place.getName(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onError(@NonNull Status status) {
        }
    };

    /**
     * Initializes the api that is used to search places from AutocompleteSupportFragment.
     */
    private void initPlaces() {
        final String apiKey = getString(R.string.google_maps_key);
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }
    }

    /**
     * Initializes ViewModel Instance for this activity.
     */
    private void initViewModel() {
        final MapsViewModel.Factory viewModelFactory
                = new MapsViewModel.Factory();

        final ViewModelProvider viewModelProvider
                = new ViewModelProvider(this, viewModelFactory);

        mapsViewModel = viewModelProvider.get(MapsViewModel.class);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        locationPermissionsManager.onRequestPermissionsResult(requestCode, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        locationPermissionsManager.onActivityResult(requestCode);
    }
}
